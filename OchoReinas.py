import random

class Poblacion(object):
    def __init__(self):
        self.PoblacionTotal = []
        self.CopiaPoblacionTotal = []

    def GenerarPoblacionInicial(self, Cantidad):
        for i in range(Cantidad):
            fila = []
            for j in range(8):
                fila.append(random.randint(1, 8))
            self.PoblacionTotal.append(fila)
        #self.PoblacionTotal.append([3,5,8,4,1,7,2,6]) #Solucion
        self.CopiaPoblacionTotal = self.PoblacionTotal[:][:]

    def ImprimirPoblacion(self):
        for i in range(len(self.PoblacionTotal)):
            for j in range(8):
                print(self.PoblacionTotal[i][j], end="\t")
            print("")

    def imprimir_poblacion_solucion(self, cadena):
        for i in range(len(self.PoblacionTotal)):
            for j in range(8):
                if i == cadena:
                    print('\033[92m'+str(self.PoblacionTotal[i][j])+'\033[0m', end="\t")
                else:
                    print(str(self.PoblacionTotal[i][j]), end="\t")
            print("")

    def CalcularFitnessFuncion(self):
        FitnessTotal = 0
        for cadena in range(len(self.PoblacionTotal)):
            ContadorAtaques = 0
            for pivote in range(8):
                if pivote != 8:
                    for recorredor in range(pivote + 1, 8):
                        #Verificar si hay reinas en la misma fila
                        if self.PoblacionTotal[cadena][pivote] == self.PoblacionTotal[cadena][recorredor]:
                            ContadorAtaques += 1
                        #Verificar si hay reinas en las diagonales
                        elif abs(self.PoblacionTotal[cadena][pivote] - self.PoblacionTotal[cadena][recorredor]) == abs(pivote - recorredor):
                            ContadorAtaques += 1
            FitnessTotal += 28 - ContadorAtaques
            if ContadorAtaques == 0:
                print("Población con solución")
                self.imprimir_poblacion_solucion(cadena)
                self.impresion_solucion(cadena)
                return False
            self.PoblacionTotal[cadena].append(28 - ContadorAtaques)
        #Calculamos el porcentaje de apareamiento
        for cadena in range(len(self.PoblacionTotal)):
            #print(self.PoblacionTotal[cadena][8], end=" ")
            self.PoblacionTotal[cadena][8] = round(self.PoblacionTotal[cadena][8] / FitnessTotal * 100, 2)
            #print(self.PoblacionTotal[cadena][8])
        return True

    def ordenar_descartar(self):
        # Ordenamos los que tengan el mayor fitness
        Promedio = 0
        for pivote in range(len(self.PoblacionTotal)):
            for recorredor in range(pivote + 1, len(self.PoblacionTotal)):
                if self.PoblacionTotal[pivote][8] < self.PoblacionTotal[recorredor][8]:
                    aux = self.PoblacionTotal[pivote]
                    self.PoblacionTotal[pivote] = self.PoblacionTotal[recorredor]
                    self.PoblacionTotal[recorredor] = aux
        # Suma del porcentaje de los fitness de todas las cadenas
        for pivote in range(len(self.PoblacionTotal)):
            Promedio += self.PoblacionTotal[pivote][8]
        Promedio = Promedio / len(self.PoblacionTotal)
        Rango = self.PoblacionTotal[0][8] - Promedio
        # Quitamos los que no cumplen expectativas
        aux1 = self.PoblacionTotal[:][:]
        contAux = 0
        for pivote2 in range(len(self.PoblacionTotal)):
            if self.PoblacionTotal[pivote2][8] < Promedio - Rango:
                aux1.pop(pivote2 - contAux)
                contAux += 1
        self.PoblacionTotal = aux1

    def seleccion_promedio_mas_alto(self):
        self.ordenar_descartar()
        #Sacamos las parejas con promedio más alto
        Criterios=[]
        for pivote in range(len(self.PoblacionTotal)):
            for recorredor in range(pivote + 1, len(self.PoblacionTotal)):
                valor = round((self.PoblacionTotal[pivote][8]+self.PoblacionTotal[recorredor][8])/2, 2) #Promedio del porcentaje del fitness de la pareja
                Criterios.append([valor, pivote, recorredor])#Lista de combinaciones de posibles parejas contiene(fitness, padre, madre)
        return Criterios

    def seleccion_n_sobre_2(self):
        self.ordenar_descartar()
        #Determinamos las parejas
        Criterios = []
        if len(self.PoblacionTotal) % 2 == 0:
            for pivote in range(len(self.PoblacionTotal)):
                Criterios.append([0, pivote, len(self.PoblacionTotal)-1-pivote])  # Lista de combinaciones de posibles parejas contiene
        else:
            for pivote in range(len(self.PoblacionTotal)):
                Criterios.append([0, pivote, len(self.PoblacionTotal)-2-pivote])  # Lista de combinaciones de posibles parejas contiene
        return Criterios

    def Crossover(self, parejas, Cantidad):
        NuevaPoblacion=[]
        flag = True
        cont=0
        #Duplicamos parejas para generen la cantidad de hijos requeridos
        while flag:
            if len(parejas) < Cantidad:
                parejas.append(parejas[cont])
                cont += 1
            else:
                flag = False
        #Intercambio de genes entre una pareja (madre, padre)
        for i in range(Cantidad):
            UbicacionBiparticion = random.randint(1, 6)
            definicion_gen = random.randint(1, 100) % 2
            if definicion_gen == 0:
                padre = 1
                madre = 2
            else:
                padre = 2
                madre = 1
            cadena_nueva = self.PoblacionTotal[parejas[i][padre]][0:UbicacionBiparticion] \
                + self.PoblacionTotal[parejas[i][madre]][UbicacionBiparticion:8]
            NuevaPoblacion.append(cadena_nueva)
        self.PoblacionTotal = NuevaPoblacion

    def Mutacion(self):
        #Verificamos en cada si puede mutar en base a la probabilidad en este caso de 10%
        for cadena in range(len(self.PoblacionTotal)):
            probabilidad_mutacion = random.randint(1,10)
            if probabilidad_mutacion == 5:#Muta si es 5
                numero_mutado= random.randint(1, 8)
                gen_escogido = random.choice(range(len(self.PoblacionTotal[0])))
                self.PoblacionTotal[cadena][gen_escogido] = numero_mutado
                #print(f"Cadena: {cadena+1}, Gen: {gen_escogido+1},  Mutacion: {numero_mutado},")

    def impresion_solucion(self, cadena):
        print("Solucion")
        for pivote in range(8):
            for recorredor in range(8):
                if pivote+1 == self.PoblacionTotal[cadena][recorredor]:
                    print("R", end="\t")
                else:
                    print("_",end="\t")
            print("")